import re
import logging.config
logging.config.fileConfig('logging.conf')
logger = logging.getLogger()


def read_word_by(keyword,text):
    search = keyword +".+"
    logging.info("search:{}".format(search))
    result = re.search(search, text)
    logging.info("result:{}".format(result))

    if result == None:
        return None
    else:
        return result.group(0).replace(keyword,"").strip()


if __name__ == '__main__':
    text =  '''
                名前:山田太郎
                メールアドレス: test@gmail.com
                ID:123
            '''
    name = read_word_by("名前:",text)
    email = read_word_by("メールアドレス:",text)
    id = read_word_by("ID:",text)

    print(name)
    print(email)
    print(id)