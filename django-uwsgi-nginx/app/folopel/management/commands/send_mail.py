from django.core.management import BaseCommand
import django, os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
django.setup()
import time, random
from django.core.mail import send_mail
from folopel.models import *
from django.utils import timezone
from folopel.backends import set_mail_server_settings


def is_target(contract: Contract, template: Template) -> bool:
    # sent check
    mails = Mail.objects.filter(customer=contract.customer, template=template)
    if mails.count() == 0:
        pass
    else:
        print("Already sent this template mail.customer:{} tempalte:{}".format(contract.customer_id,template.pk))
        return False

    # check time
    contract_start_date_time = contract.start + timezone.timedelta(hours=9)
    line_date_time = timezone.datetime.now() - timezone.timedelta(days=template.day)
    line_date = line_date_time.date()
    join_date = User.objects.get(email=template.user.email).date_joined

    # check new contract
    if join_date == contract.start:
        print("Not new contract. Contract:{}".format(contract.pk))
        return False

    # check delay mail
    if template.day == 0:
        print("Delay mail target. Contract:{}".format(contract.pk))
        wait_until = timezone.now() - timezone.timedelta(hours=template.hour, minutes=template.min)
        if timezone.now() > wait_until:
            print("True")
            return True
        else:
            print("Wait until:{}".format(wait_until))
            return False

    # check date mail
    gap = contract_start_date_time.date() - line_date
    if contract_start_date_time.date() <= line_date:
        print("Date mail target. Contract:{}".format(contract.pk))
        line_time = timezone.datetime.now().replace(hour=template.hour, minute=template.min, second=0)
        if line_time <= timezone.datetime.now():
            print("True")
            return True
        else:
            print("Wait until {}:{}".format(template.hour,template.min))
            return False
    else:
        print("Wait {} days.".format(gap.days))
        return False


def get_service(user: User):
    return Service.objects.filter(user=user)


def text_replace(text: str, user: User, contract: Contract, service: Service):
    text = text.replace("%メーリングリスト名%", service.mailing_list.name)
    text = text.replace("%ユーザー名%", user.username)
    text = text.replace("%ユーザーメールアドレス%", user.email)
    text = text.replace("%ユーザー送信用メールアドレス%", user.sending_email)
    text = text.replace("%購入者%", contract.customer.name)
    text = text.replace("%購入者メールアドレス%", contract.customer.email)
    text = text.replace("%サービス名%", service.name)
    text = text.replace("%定期購読ID%", contract.id)
    text = text.replace("%契約日%", str(contract.start.date()))
    return text


# 　メール送信履歴を保存する
def save_mail_log(user: User, customer: Customer, template: Template, mailing_list: MailingList):
    mail = Mail()
    mail.user = user
    mail.customer = customer
    mail.template = template
    mail.mailing_list = mailing_list
    mail.send_time = timezone.now()
    mail.save()

def send_mail_add_log(title,body,user,contract,template,mailing_list):
    set_mail_server_settings(user)
    try:
        result = send_mail(
            subject=title,
            message=body,
            from_email=user.sending_email,
            auth_user=user.sending_email,
            auth_password=user.sending_email_pass,
            recipient_list=[contract.customer.email],
        )
        if result == 1:
            save_mail_log(user=user, customer=contract.customer, template=template,
                          mailing_list=mailing_list)
            sleep = random.randint(1, 3)
            time.sleep(sleep)
            print("")
        else:
            print("mail send error.")
    except Exception as e:
        print(e)

def main():
    start = timezone.now()
    end = start + timezone.timedelta(days=35)
    print("range: {} - {}".format(start,end))
    users = User.objects.filter(deadline__range=[start, end])
    for user in users:
        print("Check:{}".format(user))
        services = get_service(user)

        for service in services:
            mailing_list = MailingList.objects.filter(user=user, service=service)
            contracts = Contract.objects.filter(name=service.name, user=user, status="Active")
            for contract in contracts:
                print("Check:{}".format(contract))

                for mailing in mailing_list:

                    # mailing list check
                    if mailing.stop == "1":
                        print("Stopped mailing list")
                        continue
                    else:
                        pass

                    templates = Template.objects.filter(mailing_list=mailing)
                    if templates.count() > 0:
                        for template in templates:
                            if is_target(contract, template):

                                # switch test mode
                                if os.environ.get("USER") == "trmt_8":
                                    contract.customer.email = "teranori+folopel@gmail.com"
                                else:
                                    contract.customer.email = "teranori+folopel@gmail.com"
                                    pass

                                if contract.id in mailing.exclude:
                                    print("exclude ID :{}".format(contract.id))
                                    continue

                                title = text_replace(template.title, user, contract, service)
                                body = text_replace(template.body, user, contract, service)
                                send_mail_add_log(title,body,user,contract,template,mailing)

                            else:
                                print("Skip. email:{} template:{}".format(contract.customer.email, template.pk))
                    else:
                        print("No template. mailing_list:".format(mailing.pk))


class Command(BaseCommand):
    def handle(self, *args, **options):
        main()


if __name__ == '__main__':
    main()
