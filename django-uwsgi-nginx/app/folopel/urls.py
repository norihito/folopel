"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.urls import reverse_lazy
from . import views
from django.contrib.auth import views as auth_views
from django.conf.urls import include, url


from rest_framework import routers
from .views import ContractViewSet,TransactionViewSet
app_name = 'folopel'

urlpatterns = [
    # Others
    url(r'^$', views.IndexView.as_view(), name='home'),
    # url(r'^download/(?P<type>[a-z]+)/(?P<pk>\d+)/$', views.pdf_view, name='download'),
    url(r'^test_mail/$', views.TestMailView.as_view(), name='test_mail'),
    url(r'^how_to/$', views.HowToView.as_view(), name='how_to'),
    url(r'^setting/(?P<pk>[0-9]+)$', views.SettingView.as_view(), name='setting'),

    # Template
    url(r'^template/edit/(?P<pk>[0-9]+)/$', views.TemplateEditView.as_view(), name='template_edit'),
    url(r'^template/delete/(?P<pk>[0-9]+)/$', views.TemplateDeleteView.as_view(), name='template_delete'),
    url(r'^template/make/(?P<pk>[0-9]+)/$', views.TemplateMakeView.as_view(), name='template_make'),
    url(r'^template_list/(?P<pk>[0-9]+)/$', views.TemplateListView.as_view(), name='template_list'),

    # Service List
    url(r'^service/edit/(?P<pk>[0-9]+)/$', views.ServiceEditView.as_view(), name='service_edit'),
    url(r'^service/delete/(?P<pk>[0-9]+)/$', views.ServiceDeleteView.as_view(), name='service_delete'),
    url(r'^service/make/$', views.ServiceMakeView.as_view(), name='service_make'),
    url(r'^service_list/$', views.ServiceListView.as_view(), name='service_list'),

    # Mailing List
    url(r'^mailing_list/edit/(?P<pk>[0-9]+)/$', views.MailingListEditView.as_view(), name='mailing_list_edit'),
    url(r'^mailing_list/delete/(?P<pk>[0-9]+)/$', views.MailingListDeleteView.as_view(), name='mailing_list_delete'),
    url(r'^mailing_list/make/$', views.MailingListMakeView.as_view(), name='mailing_list_make'),
    url(r'^mailing_list/$', views.MailingListView.as_view(), name='mailing_list'),


    url(r'^mail/$', views.MailView.as_view(), name='mail'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': settings.LOGOUT_URL},  name='logout'),
    #
    # # Sing up
    url(r'^signup/$', views.Signup.as_view(), name='signup'),
    url(r'^forgot/$', views.Forgot.as_view(success_url=reverse_lazy('folopel:password_reset_done')), name='forgot'),


    # # Reset password
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.PasswordResetConfirmView.as_view(success_url = reverse_lazy('folopel:password_reset_complete')), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

]
static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

router = routers.DefaultRouter()
router.register(r'contract', ContractViewSet)
router.register(r'transaction', TransactionViewSet)
