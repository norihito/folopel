from folopel.management.commands.paypal.gmail import Gmail
class Canceled(Gmail):
    html_text = None
    dom = None

    def read_price(self):
        self.set_price(self.dom.xpath("//td")[20].text)

    def read_cs_name(self):
        self.set_cs_name(self.dom.xpath("//td")[15].text)

    def read_my_name(self):
        text = self.html_text
        end_index = text.find("様")
        text = text[end_index - 30:end_index]
        start_index = text.rfind("\n") + 1
        text = text[start_index:]
        self.set_my_name(text)

    def read_cs_email(self):
        self.set_cs_email(self.dom.xpath("//td")[16].text)

    def read_subscribe(self):
        # self.set_subscribe(self.dom.xpath("//td")[17].text)
        text = self.dom.xpath("//td/text()")[25]
        self.set_subscribe(text)

    def read_test(self):
        elms = self.dom.xpath("//p")
        con = 0
        for elm in elms:
            print("{}:{}".format(con,elm.text))
            con +=1