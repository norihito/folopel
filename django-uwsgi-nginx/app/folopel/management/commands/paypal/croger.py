from folopel.management.commands.paypal.re_test import read_word_by
import logging.config
def plus(n):
    num = n
    def pluss(m):
        nonlocal num
        num = num + m
        return num
    return pluss

if __name__ == '__main__':
    f = plus(1)
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger()
    logger.setLevel(30)
    # logger.log(10,f(1))
    # logger.log(20,f(2))
    logger.debug(read_word_by("b","abcd"))