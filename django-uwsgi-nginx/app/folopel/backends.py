from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.conf import settings


class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None


def set_mail_server_settings(user):
    #  mail settings
    print("send_mail:{}".format(user.sending_email))
    # SMTP settings
    if "@yahoo" in user.sending_email:
        print("Yahoo! Mail")
        settings.EMAIL_HOST = "smtp.mail.yahoo.co.jp"
        settings.EMAIL_USE_TLS = False
        settings.EMAIL_PORT = 587


    elif "@gmail" in user.sending_email:
        print("Gmail")
        settings.EMAIL_HOST = "smtp.gmail.com"
        settings.EMAIL_USE_TLS = True
        settings.EMAIL_PORT = 587

    else:
        print("Server Mail")
        settings.EMAIL_HOST = user.host
        settings.EMAIL_USE_TLS = user.ssl
        settings.EMAIL_PORT = user.port
    print("HOST:{} TLS:{} PORT:{}".format(settings.EMAIL_HOST, settings.EMAIL_USE_TLS, settings.EMAIL_PORT))

