from django.shortcuts import render
import os
from time import strftime, gmtime
from datetime import date

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.http import Http404
from django.template.loader import get_template
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import generic
from django.contrib.auth import views as auth_views
from django.contrib import messages
from django.urls import reverse_lazy
from rest_framework.permissions import IsAuthenticated

from folopel.forms import *
from folopel.models import *
from website import settings
from django.contrib.auth import get_user_model


class IndexView(generic.TemplateView):
    template_name = 'index.html'


class HowToView(generic.TemplateView):
    template_name = 'how_to.html'


# --------------------
# Setting View
# --------------------

class SettingView(LoginRequiredMixin, generic.UpdateView):
    model = get_user_model()
    template_name = 'setting.html'
    form_class = UserForm

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        user_model = User.objects.get(email=self.request.user)
        if user_model.status == "0":
            dead_line = user_model.deadline + timezone.timedelta(hours=9)
            ctx['deadline_message'] = "ご利用可能期限：{}".format(dead_line.date())
        return ctx

    def get_success_url(self):
        messages.success(self.request, 'アカウント情報を更新しました。')
        return reverse_lazy('folopel:setting', kwargs={'pk': self.request.user.id})


# --------------------
# Template View
# --------------------


class TemplateEditView(LoginRequiredMixin, generic.UpdateView):
    model = Template
    template_name = 'template.html'
    context_object_name = 'template'
    form_class = TemplateEditForm

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'テンプレートを更新しました。')
            return reverse_lazy('folopel:mailing_list')
        else:
            messages.success(self.request, 'テンプレート更新に失敗しました。')
            return Http404


class TemplateMakeView(LoginRequiredMixin, generic.CreateView):
    model = Template
    template_name = 'template.html'
    context_object_name = 'template'
    form_class = TemplateEditForm

    def form_valid(self, form):
        pk = self.kwargs.get("pk")
        print("pk:".format(pk))
        form.instance.mailing_list = MailingList.objects.get(pk=pk)
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'テンプレートを作成しました。')
            return reverse_lazy('folopel:template_list',kwargs={'pk': self.kwargs.get("pk")})
        else:
            messages.success(self.request, 'テンプレート作成に失敗しました。')
            return Http404

class TemplateListView(LoginRequiredMixin, generic.ListView):
    model = Template
    context_object_name = 'templates'
    template_name = 'template_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['pk'] = self.kwargs.get("pk")
        return ctx
    def get_queryset(self):
        return Template.objects.filter(user=self.request.user,mailing_list = self.kwargs.get("pk")).order_by('day')


class TemplateDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Template
    template_name = 'delete.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'テンプレートを削除しました。')
            template =  Template.objects.get(pk=self.kwargs.get("pk"))
            print(template)
            return reverse_lazy('folopel:template_list',kwargs={'pk': template.mailing_list.pk})
        else:
            messages.success(self.request, 'テンプレート削除に失敗しました。')
            return Http404



# --------------------
# Service View
# --------------------


class ServiceEditView(LoginRequiredMixin, generic.UpdateView):
    model = Service
    template_name = 'service.html'
    context_object_name = 'service'
    form_class = ServiceEditForm

    def get_form(self, obj=None, **kwargs):
        form_class = super(ServiceEditView, self).get_form()
        form_class.fields['mailing_list'].queryset = MailingList.objects.filter(user_id=self.request.user.id)
        return form_class

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'サービスを更新しました。')
            return reverse_lazy('folopel:service_list')
        else:
            messages.success(self.request, '更新に失敗しました。')
            return Http404


class ServiceMakeView(LoginRequiredMixin, generic.CreateView):
    model = Service
    template_name = 'service.html'
    context_object_name = 'service'
    form_class = ServiceEditForm

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'サービスを作成しました。')
            return reverse_lazy('folopel:service_list')
        else:
            messages.success(self.request, 'テンプレート作成に失敗しました。')
            return Http404

    def get_form(self, obj=None, **kwargs):
        form_class = super(ServiceMakeView, self).get_form()
        form_class.fields['mailing_list'].queryset = MailingList.objects.filter(user_id=self.request.user.id)
        return form_class

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class ServiceListView(LoginRequiredMixin, generic.ListView):
    model = Service
    template_name = 'service_list.html'
    def get_queryset(self):
        return Service.objects.filter(user=self.request.user).order_by('pk')


class ServiceDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Service
    template_name = 'delete.html'


# --------------------
# MailingList View
# --------------------


class MailingListEditView(LoginRequiredMixin, generic.UpdateView):
    model = MailingList
    template_name = 'mailing.html'
    context_object_name = 'mailinglist'
    form_class = MailingListEditForm

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'メーリングリストを更新しました。')
            return reverse_lazy('folopel:mailing_list')
        else:
            messages.success(self.request, 'メーリングリスト更新に失敗しました。')
            return Http404


class MailingListMakeView(LoginRequiredMixin, generic.CreateView):
    model = MailingList
    template_name = 'mailing.html'
    context_object_name = 'mailinglist'
    form_class = MailingListEditForm

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'メーリングリストを作成しました。')
            return reverse_lazy('folopel:mailing_list')
        else:
            messages.success(self.request, 'メーリングリスト作成に失敗しました。')
            return Http404

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



class MailingListView(LoginRequiredMixin, generic.ListView):
    model = MailingList
    template_name = 'mailing_list.html'
    def get_queryset(self):
        return MailingList.objects.filter(user=self.request.user).order_by('pk')


class MailingListDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = MailingList
    template_name = 'delete.html'

    def get_success_url(self ,**kwargs):
        if kwargs != None:
            messages.success(self.request, 'メーリングリストを削除しました。')
            return reverse_lazy('folopel:mailing_list')
        else:
            messages.success(self.request, 'メーリングリスト削除に失敗しました。')
            return Http404


# --------------------
# Mail View
# --------------------

class MailView(LoginRequiredMixin, generic.ListView):
    model = Mail
    template_name = 'mail_list.html'
    context_object_name = 'mails'

    def get_queryset(self):
        return Mail.objects.filter(user=self.request.user,send_time__isnull=False).exclude(customer__name='unknown').order_by('pk')


# --------------------
# TestMail View
# --------------------


class TestMailView(LoginRequiredMixin, generic.TemplateView):
    model = get_user_model()
    template_name = 'test_mail.html'
    form_class = TestMailForm



    def get_context_data(self, **kwargs):
        from folopel.backends import set_mail_server_settings
        user = get_user_model().objects.get(pk=self.request.user.id)
        set_mail_server_settings(user)
        try:

            send_mail(
                subject='Folopelテストメール',
                message='Folopelをご利用いただきありがとうございます。\n{}から正常にメールの送信がされました。\n\n\nFolopel: https://Ffolopel.net/'.format(user.sending_email),
                from_email=user.sending_email,
                auth_user=user.sending_email,
                auth_password=user.sending_email_pass,
                recipient_list=[user.email],
            )

            messages.success(self.request,'{}から{}宛にテストメールを送信しました。'.format(user.sending_email,user.email))
        except:
            messages.error(self.request, '{}からテストメール送信に失敗しました。下記の解決方法をご確認いただき、再度お試しください。独自ドメインのメールアドレスでどうしても送信ができない場合は folopel.info@gmail.com にご連絡いただくか、送信用メールアドレスをGmailにご変更ください。'.format(user.sending_email),'danger')

# --------------------
# User View
# --------------------

class Forgot(auth_views.PasswordResetView):
    template_name = 'registration/forgot.html'
    email_template_name = 'registration/password_reset_message.txt'
    extra_email_context = None
    from_email = None
    html_email_template_name = None
    subject_template_name = 'registration/password_reset_subject.txt'
    token_generator = default_token_generator

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': settings.EMAIL_HOST_USER,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }

        form.save(**opts)
        return super(Forgot, self).form_valid(form)


INTERNAL_RESET_URL_TOKEN = 'set-password'
INTERNAL_RESET_SESSION_TOKEN = '_password_reset_token'


class Signup(generic.CreateView):

    model = get_user_model()
    form_class = UserCreationForm
    template_name = 'registration/sign-up.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            messages.success(self.request, 'ご登録いただきありがとうございます。<br>こちらからログインをお願いします。')
            return reverse_lazy('folopel:login')
        else:
            messages.success(self.request, '登録に失敗しました。')
            return reverse_lazy('folopel:signup')

    def form_valid(self, form):

        user = form.save()
        user.sending_email = user.email
        user.paypal_email = user.email
        user.deadline = timezone.now() + timezone.timedelta(days=30)
        user.save()

        current_site = get_current_site(self.request)
        domain = current_site.domain

        subject_template = get_template(
            'registration/mailtemplate/new/subject.txt')
        message_template = get_template(
            'registration/mailtemplate/new/message.txt')

        context = {
            'protocol': 'https' if self.request.is_secure() else 'http',
            'domain': domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user),
            'user': user,
        }

        subject = subject_template.render(context)
        message = message_template.render(context)
        from_email = settings.EMAIL_HOST_USER
        to = [user.email]

        send_mail(subject, message, from_email, to)

        return super().form_valid(form)



# --------------------
# API SETTINGS
# --------------------


import django_filters
from rest_framework import viewsets, filters
from .serializer import *


class ContractViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('start', 'end')
    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        return Contract.objects.filter(user=self.request.user)

class TransactionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    # permission_classes = (IsAuthenticated,)
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('datetime',)
    # def get_queryset(self):
    #     """
    #     This view should return a list of all the purchases
    #     for the currently authenticated user.
    #     """
    #     return Transaction.objects.filter(contract__user=self.request.user)


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

