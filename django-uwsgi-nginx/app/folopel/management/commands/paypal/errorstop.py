from folopel.management.commands.paypal.gmail import Gmail
import lxml.html
from folopel.models import Transaction
from django.utils import timezone
class ErrorStop(Gmail):
    html_text = None
    dom = None

    def read_price(self):
        text = self.dom.xpath("//td")[23].text
        self.set_price(text)

    def read_cs_name(self):
        self.set_cs_name(self.dom.xpath("//td")[17].text)

    def read_my_name(self):
        text = self.html_text
        end_index = text.find("様")
        text = text[end_index - 30:end_index]
        start_index = text.rfind("\n") + 1
        text = text[start_index:]
        self.set_my_name(text)

    def read_cs_email(self):
        self.set_cs_email(self.dom.xpath("//td")[18].text)

    def read_subscribe(self):
        self.set_subscribe(self.dom.xpath("//td")[19].text)

    def read_test(self):
        text = lxml.html.tostring(self.dom)
        elms = self.dom.xpath("//td")
        con = 0
        for elm in elms:
            print("{}:{}".format(con,elm.text))
            con +=1

    def read_service_name(self):
        self.set_service_name(self.dom.xpath("//td")[21].text)