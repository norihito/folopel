from django.core.management import BaseCommand
import django,os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
django.setup()
from folopel.management.commands.paypal.gmail import Gmail
from folopel.management.commands.gmail import *
from folopel.management.commands.paypal.contract import Contract
from folopel.management.commands.transport import Transport
from folopel.models import Mail,Customer
import logging.config

START = 0
LOG_LEVEL = 10

def read_mail_content(content):
    transactions = ["Active","Cancel","Canceled","ErrorStop","Error"]
    # try:
    gmail = Gmail(content)
    if not gmail.is_expect_mail():
        logging.debug(gmail.mail_type)

        if gmail.mail_type == "Contract":
            contract = Contract(content)
            # contract.read_test()
            contract.read_service_name()
            contract.read_cs_name()
            contract.read_cs_email()
            contract.read_my_name()
            contract.read_datetime()
            contract.read_next_payment()
            contract.read_price()
            contract.read_subscribe()
            contract.to_string()
            # DB
            contract.make_service()
            contract.save_transaction()

        elif gmail.mail_type in transactions:
            logging.debug(gmail.title)
            gmail.read_cs_name()
            gmail.read_my_name()
            gmail.read_cs_email()
            gmail.read_subscribe()
            gmail.read_service_name()
            gmail.read_transaction()
            gmail.read_datetime()
            if gmail.mail_type in "Active":gmail.read_next_payment()
            gmail.read_price()
            gmail.to_string()
            # DB
            gmail.update_subscribe()
            gmail.make_service()
            gmail.save_transaction()

            if not gmail.mail_type in ["Active","Contract"]:
                logging.debug("send special mail")
                gmail.send_special_mail()

        elif gmail.mail_type == "Transport":
            transport = Transport(content)
            if Mail.objects.filter(id = transport.mail_id).count() == 0:
                no = transport.read_transport_no()
                email = transport.read_transport_email()
                transport.send_transport_email(no,email)
                customer = Customer.objects.get_or_create(name=email,email=email)[0]
                transport.save_mail(customer)

        else:
            logging.info("This mail doesn't match with any pattern.")
            logging.info(read_snippet(content))




def main():

    logging.config.fileConfig('read_mail_log.conf')
    logger = logging.getLogger()
    logger.setLevel(LOG_LEVEL)
    user = 'me'

    json_path = './client_secret_626154498738-l0qmanntp7ghmlkmr521s1ote3qmfnla.apps.googleusercontent.com.json'
    api = GmailApi(json_path) #You need authorized when you run at first time.
    query =  "service-jp@paypal.com OR 転送の確認 "#paypal mail

    # -------------------
    #  mail test query
    # -------------------

    # query =  "subject:お支払いがありました"# Active
    # query =  "subject:キャンセルされました"# Canceled
    # query =  "subject:キャンセルに関する"# Cancel
    # query =  "subject:中断されました"# ErrorStop
    # query =  "subject:支払いが実行されませんでした"# Error
    # query =  "subject:締結"#Unread message query
    # query =  "subject:Gmail の転送の確認"#Unread message query

    mail_list = api.get_mail_list(user, query )
    # logging.info( json.dumps(maillist, indent=4))
    mail_list_size = len(mail_list["messages"])
    logging.debug("mail list size:{}".format(mail_list_size))


    for i in range(START,mail_list_size):# start from
        logging.info(" - - - {} - - -".format(i))
        # logging.info( json.dumps(content, indent=4))

        message_no = mail_list["messages"][i]
        mail_con = Mail.objects.filter(id = message_no['id']).count()
        if mail_con == 0:
            content = api.get_mail_content(user, message_no['id'])
            read_mail_content(content)
        else:
            logging.info("Skip {}".format(message_no['id']))


class Command(BaseCommand):
    def handle(self, *args, **options):
        main()

if __name__ == '__main__':
    main()
