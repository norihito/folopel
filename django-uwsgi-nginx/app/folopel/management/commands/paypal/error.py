from folopel.management.commands.paypal.gmail import Gmail
class Error(Gmail):

    def read_cs_name(self):
        text = str(self.dom.xpath("//p")[0].text_content())
        s_text = "買い手の名前:"
        start_index = text.find(s_text)+len(s_text)
        text = text[start_index:]
        e_text = text.find("\r\n")
        self.set_cs_name(text[:e_text])

    def read_cs_email(self):
        text = str(self.dom.xpath("//p")[0].text_content())
        s_text = "買い手のメールアドレス:"
        start_index = text.find(s_text)+len(s_text)
        text = text[start_index:]
        e_text = text.find("\r\n")
        self.set_cs_email(text[:e_text])

    def read_service_name(self):
        self.set_service_name("")

    def read_my_name(self):
        text = str(self.dom.xpath("//p")[0].text_content())
        end_index = text.find("様")
        text = text[0:end_index].strip()
        self.set_my_name(text)

    def read_subscribe(self):
        text = str(self.dom.xpath("//p")[0].text_content())
        start_index = text.find("I-")
        self.set_subscribe(text[start_index:start_index+14])

    def read_datetime(self):
        self.set_datetime(self.dom.xpath("//span")[2].text)

    def read_test(self):
        elms = self.dom.xpath("//p")
        con = 0
        for elm in elms:
            print("{}:{}".format(con,elm.text))
            con +=1

    def read_next_payment(self):
        self.set_next_payment(self.dom.xpath("//td")[27].text)
