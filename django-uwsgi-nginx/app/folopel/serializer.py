# coding: utf-8

from rest_framework import serializers
from .models import *


class CustomerSerializer(serializers.ModelSerializer):
    read_only_fields= ('name','email')
    class Meta:
        model = Customer
        fields = ('name','email')

class ContractSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    class Meta:
        model = Contract
        fields = ('id', 'name','status','start','end','customer',)

class TransactionSerializer(serializers.ModelSerializer):
    contract = ContractSerializer()
    class Meta:
        model = Transaction
        fields = ('datetime', 'id', 'mail','contract','price','next_payment','status')