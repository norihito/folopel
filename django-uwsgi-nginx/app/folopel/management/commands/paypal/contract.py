from folopel.management.commands.paypal.gmail import Gmail
import lxml.html

class Contract(Gmail):

    def read_cs_name(self):
        self.set_cs_name(self.dom.xpath("//td")[15].text)

    def read_service_name(self):
        text = self.find_text("対象:","配送先",self.html_text)
        if len(text)>40:
            text = self.find_text("対象:","自動支払",self.html_text)
        self.service_name = text

    def read_my_name(self):
        text = self.html_text
        end_index = text.find("様")
        text = text[end_index - 30:end_index]
        start_index = text.rfind("\n") + 1
        text = text[start_index:]
        self.set_my_name(text)

    def read_cs_email(self):
        self.set_cs_email(self.dom.xpath("//td")[16].text)

    def read_subscribe(self):
        self.set_subscribe(self.dom.xpath("//td")[17].text)

    def read_datetime(self):
        self.set_datetime(self.dom.xpath("//td")[23].text)

    def read_test(self):
        text = lxml.html.tostring(self.dom)
        elms = self.dom.xpath("//td")
        con = 0
        for elm in elms:
            print("{}:{}".format(con,elm.text))
            con +=1

    def read_next_payment(self):
        self.set_next_payment(self.dom.xpath("//td")[23].text)

