import requests
import json
from django.core.management import BaseCommand
import django, os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
django.setup()

from folopel.models import *
from django.utils import timezone
import logging.config

logging.config.fileConfig('api/contract_update.conf')
logger = logging.getLogger()
logger.setLevel(20)
def get_tocken(email, password, token_url):
    try:
        r = requests.post(token_url, data={"username": email, "password": password})
        token = r.json()["token"]
        # print(token)
        token = {'Authorization': 'JWT {}'.format(token)}
        # print(token)
        return token
    except Exception as e:
        logger.error(e)
        logger.error("Maybe server is offline.")
        return None


def update_users_status(users, status):
    for user in users:
        # Update status
        user.status = status
        user.save()


def main(domain="https://folopel.com/"):
    token_url = domain + "token"
    transaction_url = domain + "api/transaction/?format=json&ordering=-datetime"

    # User info
    email = "folopel.info@gmail.com"
    password = "uyzw8khc"

    token = get_tocken(email, password, token_url)
    r2 = requests.get(transaction_url, headers=token)
    logger.debug(r2)

    for text in r2.json()["results"]:
        logger.info("\n- - - "+text["contract"]["status"])
        logger.info(text)
        status = text["contract"]["status"]
        cs_email = text["contract"]["customer"]["email"]

        users = User.objects.filter(paypal_email=cs_email)
        if status == "active":
            for user in users:
                # Update deadline nad status
                format = '%Y-%m-%d'
                nex_payment = timezone.datetime.strptime(text["next_payment"], format)
                user.deadline = nex_payment + timezone.timedelta(days=5)
                user.status = 1
                user.save()

        elif status == "canceled" or status == "stop" or status == "disable":
            update_users_status(users, 0)

        elif status == "error":
            pass


class Command(BaseCommand):
    def handle(self, *args, **options):
        main()


if __name__ == '__main__':

    main(domain="http://127.0.0.1:8080/")
