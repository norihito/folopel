from folopel.management.commands.paypal.gmail import Gmail
import lxml.html
from folopel.models import Transaction
from django.utils import timezone
class Cancel(Gmail):
    html_text = None
    dom = None

    def read_price(self):
        text = self.dom.xpath("//span/text()")[3]
        start_index = text.find(" ") + 1
        self.set_price(text[start_index:])

    def read_datetime(self):
        text = self.dom.xpath("//span/text()")[1]
        start_index = text.find(" ") + 1
        self.set_datetime(text[start_index:])

    def read_cs_name(self):
        text = self.dom.xpath("//p/text()")[4]
        start_index = text.find(":") + 1
        self.set_cs_name(text[start_index:])

    def read_my_name(self):
        text = self.dom.xpath("//span/text()")[0]
        text = str(text).replace("様","").strip()
        self.set_my_name(text)

    def read_cs_email(self):

        text = self.dom.xpath("//p/text()")[5]
        start_index = text.find(":") + 1
        self.set_cs_email(text[start_index:])

    def read_subscribe(self):
        text = self.dom.xpath("//p/text()")[2]
        start_index = text.find(" ") + 1
        self.set_subscribe(text[start_index:])

    def read_test(self):
        elms = self.dom.xpath("//p/text()")
        con = 0
        for elm in elms:
            print("{}:{}".format(con,elm))
            con +=1

    def read_service_name(self):
        text = self.dom.xpath("//p/text()")[3]
        start_index = text.find(": ") + 1
        self.set_service_name(text[start_index:])
