from django.contrib import admin
from .models import *
# Register your models here.


class UserAdmin(admin.ModelAdmin):
    fields = ['username', 'email', 'paypal_email','sending_email', 'sending_email_pass']


admin.site.register(User, UserAdmin)


class CustomerAdmin(admin.ModelAdmin):
    fields = ['name', 'email']

admin.site.register(Customer, CustomerAdmin)


class TemplateAdmin(admin.ModelAdmin):
    fields = ['user','day','hour','min','title', 'body']
    list_display = ('user','day','hour','min','title')


admin.site.register(Template, TemplateAdmin)



class MailAdmin(admin.ModelAdmin):

    fields = ['id','user','customer','template','mailing_list','date_time','send_time']
    readonly_fields = ['id']
    list_display = ('id','user','customer','template','mailing_list','date_time','send_time')

admin.site.register(Mail, MailAdmin)


class MailingListAdmin(admin.ModelAdmin):
    fields = ['name','exclude','stop']
    list_display = ('name','exclude','stop')

admin.site.register(MailingList, MailingListAdmin)



class ServiceAdmin(admin.ModelAdmin):
    fields = ['user','mailing_list','name']
    list_display = ('user','mailing_list','name')

admin.site.register(Service, ServiceAdmin)



class ContractAdmin(admin.ModelAdmin):
    fields = ['id','start','end','status','user','customer','name']
    list_display = ('id','start','end','status','user','customer','name')

admin.site.register(Contract, ContractAdmin)

class TransactionAdmin(admin.ModelAdmin):
    fields = ['mail','id','contract','price','datetime','next_payment','status']
    list_display = ('mail','id','contract','price','datetime','next_payment','status')

admin.site.register(Transaction, TransactionAdmin)
