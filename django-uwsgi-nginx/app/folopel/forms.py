from django import forms
from django.forms import TextInput
from django.contrib.auth.forms import UserChangeForm, ReadOnlyPasswordHashField, PasswordChangeForm, UserCreationForm
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model

from folopel.models import *


class UserCreationForm(UserCreationForm):
    email = forms.EmailField(label=("メールアドレス"), required=True)

    class Meta:
        model = get_user_model()
        fields = ["email"]

    def __init__(self, *args, **kargs):
        super(UserCreationForm, self).__init__(*args, **kargs)
        self.fields['email'].label = "メールアドレス"
        self.fields['password1'].label = "パスワード"
        self.fields['password1'].help_text = """パスワードは半角英数字8文字以上で入力してください。"""
        self.fields['password2'].label = "パスワード再入力"

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            get_user_model().objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(("このメールアドレスは既に登録されています。"))

    def clean_username(self):
        self.fields["username"] = self.cleaned_data["email"]
        print('self.fields["username"]' + self.fields["username"])
        return self.fields["username"]


class UserForm(forms.ModelForm):
    sending_email_pass = forms.CharField(widget=forms.PasswordInput(render_value=True))

    class Meta:
        model = get_user_model()
        fields = ["username","email", "paypal_email", "sending_email", "sending_email_pass", "host", "ssl", "port", ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = "ユーザー名"
        self.fields['paypal_email'].label = "PayPalメールアドレス"
        self.fields['sending_email'].label = "送信用メールアドレス"
        self.fields['sending_email_pass'].label = "送信用メールパスワード"
        self.fields['host'].label = "SMTPホスト名/送信用サーバー名"
        self.fields['ssl'].label = "SSL/TSL"
        self.fields['port'].label = "ポート"
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"


class TemplateEditForm(forms.ModelForm):
    class Meta:
        model = Template
        fields = ['title', "body", "day", "hour","min"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].label = "件名"
        # self.fields['mailing_list'].label = "メーリングリスト"
        # self.fields['mailing_list'].initial = kwargs.get("pk")
        self.fields['body'].label = "本文"
        self.fields['body'].widget.attrs.update({'rows' : '20'})
        self.fields['day'].label = "何日後に送信しますか？"
        self.fields['hour'].label = "何時に送信しますか？"
        self.fields['min'].label = "何分に送信しますか？"
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"


class ServiceEditForm(forms.ModelForm):
    class Meta:
        model = Service

        fields = ['name',"mailing_list"]
        # exclude = ('user',)

    def __init__( self, *args, **kwargs):
        super(ServiceEditForm,self).__init__(*args, **kwargs)
        self.fields['name'].label = "サービス名・商品名"
        self.fields['mailing_list'].label = "使用するメーリングリスト"

        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"

class MailingListEditForm(forms.ModelForm):
    class Meta:
        model = MailingList
        fields = ['name','stop','exclude']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].label = "メーリングリスト名（サービス・商品名）"
        self.fields['stop'].label = "配信"
        self.fields['exclude'].label = "送信除外ID"
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"


class TestMailForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["email"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"