Folopelをご利用いただきありがとうございます。

下記URLよりパスワードを再設定いただけます。
{{ protocol}}://{{ domain }}{% url 'folopel:password_reset_confirm' uidb64=uid token=token %}

どうぞよろしくお願い致します。