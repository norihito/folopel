from typing import Any



name = None

class User():

    def __init__(self):
        pass

    name = None  # type: str
    email = None # type: str

    def get_name(self) -> str:
        return self.name

    def set_name(self, name:str=None) -> str:
        self.name = name

    def get_email(self) -> str:
        return self.email

    def set_email(self, email:str=None) -> str:
        self.email = email