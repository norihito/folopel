import re
from folopel.management.commands.paypal.gmail import Gmail
from folopel.management.commands.gmail import read_snippet
from folopel.models import User
from django.core.mail import send_mail
from django.template.loader import get_template
from website import settings
from folopel.models import Mail

class Transport(Gmail):
    snippet = None

    def __init__(self,content):
        self.owner = User.objects.get(email=settings.EMAIL_HOST_USER)
        self.snippet = read_snippet(content)
        self.mail_id = self.read_mail_id(content)

    def read_transport_no(self):
        pattern = r'\D'
        return re.sub(pattern,'' , self.snippet)[0:9]

    def read_transport_email(self):
        texts = self.snippet.replace("folopel.info@gmail.com","").split()
        for text in texts:
            if "@" in text:
               return text

    def send_transport_email(self,no,email):
        subject_template = get_template('registration/mailtemplate/transport/subject.txt')
        message_template = get_template('registration/mailtemplate/transport/message.txt')

        context = {
            'no': no,
        }
        subject = subject_template.render(context)
        message = message_template.render(context)
        from_email = settings.EMAIL_HOST_USER
        to = [email]

        send_mail(subject, message, from_email, to)
        print("Sent transport confirm mail.")