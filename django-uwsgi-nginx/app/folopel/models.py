from django.db import models

# Create your models here.
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.conf import settings


MODE_LIST = (
    ( '0','未契約'),
    ( '1','契約中'),
)

TYPE_LIST = (
    ( '1','定期購読'),
    ( '0','買い切り'),
)

STOP_LIST = (
    ( '1','無効'),
    ( '0','有効'),
)

def deadline():
    return timezone.now()+timezone.timedelta(days=35)


class User(AbstractUser):
    username = models.CharField( ('ユーザー名'), default='',max_length=150,unique=False )
    email = models.EmailField(('メールアドレス'), unique=True )
    paypal_email = models.EmailField(('PayPalアドレス'), unique=True )
    sending_email = models.CharField( max_length=150,default='', blank=True)
    sending_email_pass = models.CharField( max_length=150,default='', blank=True)
    port = models.CharField(('ポート'), max_length=3,default='587', blank=True)
    host = models.CharField(('ホスト/サーバー'), max_length=150, unique=False, blank=True)
    ssl = models.BooleanField(default=True)
    status = models.CharField(max_length=10, choices=MODE_LIST, default='1' ,blank=True)
    deadline = models.DateTimeField(default=deadline)

    def __str__(self):
        return "{}".format(self.email)

    class Meta(object):
        unique_together = ('email',)


class Customer(models.Model):
    name = models.CharField( ('名前'), default='',max_length=150)
    email = models.EmailField(('メールアドレス'), unique=True )

    def __repr__(self):
        return "{}: {}".format(self.name, self.email)

    __str__ = __repr__

class MailingList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(("メーリングリスト"),max_length=150)
    stop = models.CharField(max_length=10, choices=STOP_LIST, default='1' ,blank=False)
    exclude = models.CharField(("配信除外ID"), default='',max_length=255,blank=True)

    def __str__(self):
        return self.name

class Service(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mailing_list = models.ForeignKey(MailingList,on_delete=models.CASCADE)
    name = models.CharField(("サービス名"),max_length=150)

    def __str__(self):
        return self.name


class Template(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,default=1,on_delete=models.CASCADE)
    mailing_list = models.ForeignKey(MailingList,on_delete=models.CASCADE)
    day = models.IntegerField(null=False)
    hour = models.IntegerField(default=18,null=True)
    min =  models.IntegerField(default=0,null=True)
    title = models.CharField(max_length=150)
    body = models.TextField(default='')

    def __str__(self):
        # text = "{} {}日後 {}時に送信 : {}".format(self.mailing_list.name,self.day,self.time,self.title)
        return self.title

class Mail(models.Model):
    id =  models.CharField( ('メールID'),max_length=32,default=timezone.now,primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,default=1,on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer,default=1,on_delete=models.CASCADE)
    template = models.ForeignKey(Template,default=1,on_delete=models.CASCADE)
    mailing_list = models.ForeignKey(MailingList,default=1,on_delete=models.CASCADE)
    date_time = models.DateTimeField(default=timezone.now)
    send_time = models.DateTimeField(default=None,null=True)

    def __str__(self):
        # return "{}:{} from: {} to: {}".format(self.date_time,self.template.title,self.user.email,self.customer)
        # return "{}:{} to: {}".format(self.date_time,self.template.title,self.customer)
        return str(self.date_time)


class Contract(models.Model):
    id = models.CharField( ('定期購読ID'),max_length=150,primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    name = models.CharField(("サービス名"),default='',max_length=150)
    status = models.CharField(("ステータス"),max_length=150)
    start = models.DateTimeField( ('開始日'),default=timezone.now)
    end = models.DateTimeField( ('終了日'),default=None,null=True)

    def __str__(self):
        return self.id

class Transaction(models.Model):
    id = models.CharField( ('トランザクションID'),max_length=17,default='0',primary_key=True)
    mail = models.ForeignKey(Mail,on_delete=models.CASCADE)
    contract = models.ForeignKey(Contract,on_delete=models.CASCADE)
    price = models.CharField(default=0,max_length=15)
    datetime = models.DateTimeField()
    next_payment = models.DateField(null=True)
    status = models.CharField(("ステータス"),max_length=15)

    def __str__(self):
        return self.id

