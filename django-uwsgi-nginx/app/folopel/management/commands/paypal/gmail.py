import lxml.html
from folopel.management.commands.gmail import *
from folopel.models import User, Template
from folopel.models import Customer, Contract, Transaction, Mail, Service, MailingList
from django.utils import timezone
import pytz,os
from ..send_mail import send_mail_add_log, text_replace
import re
import logging.config
from folopel.models import Contract as ContractTable


class Gmail():
    mail_id = None  # type:str
    _html_text = None  # type:str
    service_name = None  # type: str
    _service = None  # type: Service
    cs_email = None  # type: str
    cs_name = None  # type: str
    customer = None  # type: Customer
    my_name = None  # type: str
    my_email = None  # type: str
    subscribe = None  # type: Contract
    currency = None  # type: str
    end = None  # type: timezone
    transaction = None  # type: str
    datetime = None  # type: timezone
    next_payment = None  # type: timezone
    owner = None  # type: User
    _title = None  # type:str
    _dom = None
    _mail_type = None

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def end(self):
        return self.end

    @end.setter
    def end(self, value):
        self.end = value

    @property
    def mail_type(self):
        return self._mail_type

    @mail_type.setter
    def mail_type(self, value):
        self._mail_type = value

    @property
    def dom(self):
        return self._dom

    @dom.setter
    def dom(self, value):
        self._dom = value

    @property
    def html_text(self):
        return self._html_text

    @html_text.setter
    def html_text(self, value):
        self._html_text = self.remove_html_tags(value)

    def get_service_name(self) -> str:
        return self.service_name

    def get_cs_email(self) -> str:
        return self.cs_email

    def set_cs_email(self, cs_email: str = None) -> str:
        self.cs_email = cs_email
        logging.debug(self.cs_email)

    def get_subscribe(self) -> Contract:
        return self.subscribe

    def set_subscribe(self, subscribe: str = None):
        if self.owner is None:
            logging.info("[Error]Owner is None.")
            return
        # logging.info(subscribe)
        contracts = Contract.objects.filter(id=subscribe)
        if contracts.count() > 0:
            self.subscribe = contracts[0]
        else:
            contract = Contract()
            contract.id = subscribe
            contract.user = self.owner
            contract.customer = self.find_customer(cs_name=self.cs_name, cs_email=self.cs_email)
            self.subscribe = contract
            contract.save()

    def get_datetime(self) -> timezone:
        return self.datetime

    def set_datetime(self, datetime: str = None):
        datetime = self.kanji_datetime_to_symbol(datetime)
        date_format = "%Y/%m/%d"
        jst = pytz.timezone('Japan')
        if "JST" in datetime:
            logging.debug("datetime;{}".format(datetime))
            date_format = "%Y/%m/%d %H:%M:%S %Z"
        datetime = timezone.datetime.strptime(datetime, date_format)
        datetime = datetime.replace(tzinfo=jst)
        self.datetime = datetime

    def get_next_payment(self) -> timezone:
        return self.next_payment

    def set_next_payment(self, next_payment: str = None):
        date_format = "%Y/%m/%d"
        next_payment = self.kanji_datetime_to_symbol(next_payment)
        next_payment = timezone.datetime.strptime(next_payment, date_format)
        self.next_payment = next_payment

    def get_owner(self) -> User:
        return self.owner

    def set_owner(self, owner: str = None):
        self.owner = owner

    def get_cs_name(self) -> str:
        return self.cs_name

    def set_cs_name(self, cs_name: str = None):
        self.cs_name = cs_name

    def get_my_name(self) -> str:
        return self.my_name

    def set_my_name(self, my_name: str = None):
        self.my_name = my_name

    def get_my_email(self) -> str:
        return self.my_email

    def set_my_email(self, my_email: str = None):
        self.my_email = my_email

    def get_transaction(self) -> str:
        return self.transaction

    def set_transaction(self, transaction: str = None):
        self.transaction = transaction


    # --------------
    #   Initialize
    # --------------
    def __init__(self, content):
        self.mail_id = self.read_mail_id(content)
        self.owner = self.read_from(content)
        self.title = read_title(content)
        message = read_message(content)
        # logging.debug(message)
        self.set_dom(message)
        self.html_text = message
        self.classify_mail_type()

    # --------------
    #    Switch
    # --------------

    def classify_mail_type(self):
        if "お客さまへの自動支払いはキャンセルされました" in self._title:
            self.mail_type = "Canceled"
        elif "キャンセルに関するご連絡" in self._title:
            self.mail_type = "Cancel"
        elif "お支払いがありました" in self.title:
            self.mail_type = "Active"
        elif "締結" in self.title:
            self.mail_type = "Contract"
        elif "中断されました" in self._title:
            self.mail_type = "ErrorStop"
        elif "支払いが実行されませんでした" in self.title:
            self.mail_type = "Error"
        elif "転送の確認" in self.title:
            self.mail_type = "Transport"
        else:
            logging.info("Didn't mtch any pattern.")
            self.mail_type = None


    # --------------
    #     Mail
    # --------------

    def set_dom(self, message):
        self.dom = lxml.html.fromstring(message)

    def remove_html_tags(self,text):
        start = text.find("<body")
        if start > 0:text = text[start:]
        p = re.compile(r"<[^>]*?>")
        return p.sub("", text)

    def print_mail_type(self):
        logging.info("MailType:{}".format(self.mail_type))


    def is_expect_mail(self):
        keywords = ["Forwarded message"]
        for keyword in keywords:
            if keyword in self.html_text:
                logging.debug("{} is error keyword".format(keyword))
                return True
            else:
                return False

    # --------------
    # Send Special Mail
    # --------------

    def send_special_mail(self):
        template = None
        service = None
        mailing_list = None
        try:
            service = Service.objects.get(user=self.owner, name=self.subscribe.name)
            mailing_list = MailingList.objects.get(user=self.owner, service=service)

            if self._mail_type == "Cancel":
                template = Template.objects.get(user=self.owner, day=888, mailing_list=mailing_list)
            elif self._mail_type == "Canceled":
                template = Template.objects.get(user=self.owner, day=999, mailing_list=mailing_list)
            elif self._mail_type == "ErrorStop":
                template = Template.objects.get(user=self.owner, day=777, mailing_list=mailing_list)
            elif self._mail_type == "Error":
                template = Template.objects.get(user=self.owner, day=666, mailing_list=mailing_list)

            end_day = timezone.now()
            start_day = end_day - timezone.timedelta(days=6)
            mails = Mail.objects.filter(template=template, user=self.owner, date_time__range=(start_day, end_day))

            if mails.count() > 0:
                logging.debug("Alerady sent mail.{}".format(template.title))
                return

            # if os.getlogin() == "trmt_8":
            self.subscribe.customer.email = "folopel.info@gmail.com"
            self.owner.sending_email = "folopel.info@gmail.com"
            self.owner.sending_email_pass = "Gmail.nori3"

            title = text_replace(template.title, self.owner, self.subscribe, service)
            body = text_replace(template.body, self.owner, self.subscribe, service)
            send_mail_add_log(title, body, self.owner, self.subscribe, template, mailing_list)
        except Exception as e:
            logging.error(e)
            logging.error("in Special mail user:{},service:{},mailing_list:{},template:{},".format(self.owner,service,mailing_list,template))

    # --------------
    # Read Bass Logic
    # --------------


    def find_text_start_from(self, keyword, text):
        regular = keyword +".+"
        result = re.search(regular, text)
        if result == None:
            return None
        else:
            return result.group(0).replace(keyword,"").strip()

    def regular_search(self,regular,text):
        result = re.search(regular, text)
        if result == None:
            return None
        else:
            return result.group(0).strip()

    def find_text(self,start,end,text):
        text = self.find_text_start_from(start,text)
        if text != None:
            end = text.find(end)
            if end > 0:
                text = text[:end]
        return text




    # --------------
    #     Read
    # --------------

    def read_mail_id(self, content):
        return content['id']



    def read_from(self, content) -> User:
        users = User.objects.all()
        text = content['payload']['headers']
        for user in users:
            con = str(text).find(user.paypal_email)
            if con > -1:
                return user
        logging.info("Didn't match with any addres.")
        return None


    def read_transaction(self):
        id = self.regular_search("[0-9A-Z]{17}",self.html_text)
        self.transaction = id

    def read_cs_name(self):
        name = self.find_text("顧客名:","顧客", self.html_text)
        if name == None:
            name = self.find_text("買い手の名前:","買い手", self.html_text)
        self.cs_name = name

    def read_cs_email(self):
        email = self.regular_search("([a-z0-9\.\-\_\+]+)@([a-z0-9\.]+)",self.html_text)
        self.cs_email = email

    def read_price(self):

        text = self.find_text("受け取り金額:","対象",self.html_text)
        if text == None: text = self.find_text("毎月1回の支払金額:","前回支払い日",self.html_text)
        if text == None: text = self.find_text("サイクルの支払い金額:","請求サイクル",self.html_text)
        self.price = "" if text == None else re.sub(r'\D','',text)

    def read_service_name(self):
        # text = self.find_text("顧客のメールアドレス:","個人設定",self.html_text)
        text = self.regular_search("から.+の代金のお支払い",self.html_text)
        if text != None:
            text = text.replace("から","").replace("の代金のお支払い","")
        if text == None:
            text = self.find_text("対象:","買い手",self.html_text)
        self.service_name = text if text != None else ""

    def read_my_name(self):
        text = self.regular_search("\n.+様",self.html_text).replace("様","").strip()
        self.my_name = text

    def read_subscribe(self):
        text = self.regular_search("I-.{12}",self.html_text)
        if text == None:
            logging.error("read_subscribe is None")
        self.set_subscribe(text)

    def read_datetime(self):
        date = self.regular_search("[0-9]{4}年[0-9]{1,2}月[0-9]{1,2}日",self.html_text)
        if date == None: date = self.regular_search("[0-9]{4}/[0-9]{1,2}/[0-9]{1,2} [0-9]{2}:[0-9]{2}:[0-9]{2} JST",self.html_text)
        self.set_datetime(date)

    def read_next_payment(self):
        date = self.find_text_start_from("次回の支払い期日:", self.html_text)
        date = self.kanji_datetime_to_symbol(date)
        self.set_next_payment(self.regular_search("[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}",date))

    def kanji_datetime_to_symbol(self ,text):
        return text.replace("年","/").replace("月","/").replace("日","") if not text == None else None
    # --------------
    #     SAVE
    # --------------

    def save_customer(self, cs_name, cs_email):
        customer = Customer(name=cs_name, email=cs_email)
        customer.save()

    def save_transaction(self):
        if self.owner is None:
            logging.error("Owner is None.")
            return
        mail = self.save_mail(self.subscribe.customer)

        # Transaction Table
        transaction = Transaction()
        transaction.mail = mail
        if self.transaction == None:
            transaction.id = self.mail_id.strip()
        else:
            transaction.id = self.transaction.strip()
        transaction.contract = self.subscribe
        transaction.price = self.price
        transaction.datetime = timezone.now()
        transaction.status = self.mail_type
        transaction.next_payment = self.next_payment
        transaction.save()

    def save_mail(self, customer=None):
        mail = Mail()
        mail.id = self.mail_id
        mail.user = self.owner
        mail.mailing_list_id = 1
        mail.template_id = 1
        mail.customer = customer
        mail.save()

        return mail

    def save_contract(self):
        self.save_mail(self.subscribe.customer)

        contract_table = ContractTable()
        contract_table.id = self.subscribe
        contract_table.user = self.owner
        contract_table.customer = self.find_customer(cs_email = self.cs_email,cs_name=self.cs_name)
        contract_table.status = self.mail_type
        contract_table.next_payment = self.next_payment
        contract_table.start = self.datetime
        contract_table.name = self.service_name
        contract_table.price = self.price
        contract_table.save()

    def update_subscribe(self):

        if self.subscribe == None:
            logging.error("subscribe is None.")
            return

        elif self.subscribe.name == "":
            contract = self.subscribe
            contract.customer = self.find_customer(self.cs_email, self.cs_name)
            contract.user = self.owner
            contract.status = self.mail_type
            contract.start = self.owner.date_joined
            if self.mail_type in ["Cancel", "Canceled", "Stop"] and self.subscribe.end == None:
                contract.end = timezone.now()
            contract.name = self.service_name
            contract.price = self.price
            contract.save()
            self.subscribe = contract
        else:
            self.subscribe.status = self.mail_type
            if self.mail_type in ["Cancel", "Canceled", "Stop"] and self.subscribe.end == None:
                self.subscribe.end = timezone.now()
            self.subscribe.save()


    def make_service(self):
        if self.owner is None:
            logging.error("[Error]owner is None.")
            return
        services = Service.objects.filter(user=self.owner, name=self.service_name)
        if services.count() == 0:
            service = Service()
            service.user = self.owner
            service.name = self.service_name
            service.mailing_list = MailingList.objects.get(pk=1)
            service.save()
        else:
            pass

    def find_customer(self, cs_email, cs_name):
        cs = Customer.objects.filter(email=cs_email)
        if cs.count() == 0:
            cs = Customer.objects.filter(name=cs_name)
            if cs.count() == 0:
                cs = Customer()
                cs.email = cs_email
                cs.name = cs_name
                cs.save()
                return cs
            else:
                return cs[0]
        else:
            return cs[0]




    def to_string(self):
        logging.info(
            '''
                mail_type: {}
                mail_id: {}
                service_name: {}
                cs_email: {}
                cs_name: {}
                my_name: {}
                price: {}
                subscribe: {}
                transaction: {}
                datetime: {}
                next_payment: {}
            '''.format(
                self._mail_type,
                self.mail_id,
                self.service_name,
                self.cs_email,
                self.cs_name,
                self.my_name,
                # self.owner.email,
                self.price,
                self.subscribe,
                self.transaction,
                self.datetime,
                self.next_payment,
            )
        )
